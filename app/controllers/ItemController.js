app.controller('AdminController', function(dataFactory,$scope,$http){
 
  $scope.data = [];
  dataFactory.httpRequest('itemsAll').then(function(data) {
      $scope.itemAll = data.Alldata;
    });
   
});



// Login controller-------------------------------------->

app.controller('LoginController', function(dataFactory,$scope,$http,$window)
 {  

  $scope.saveAdd = function(){
    dataFactory.httpRequest('itemsCreate','POST',{},$scope.form).then(function(data) {
      if(data.userlogin==1)
      {
        localStorage.setItem('usersession', JSON.stringify(data.userid));
        $('#logout').css('display','block');
         $('#Login').css('display','none');
        $window.history.back();
      }else if (data.userlogin==0) {

        $scope.error="Username Or Password Wrong."
      }
      //console.log(data.userlogin);
      
    });
  }



 });

// Login Controller -----------------------------

app.controller('AllLabTestController', function(dataFactory,$scope,$http){
 
  $scope.data = [];
  $scope.isDisabled = false;
  dataFactory.httpRequest('itemsAll').then(function(data) {
      $scope.itemAll = data.Alldata;
    });
  $scope.searchDB =function(){
      if($scope.searchText.length >= 3){ 

         dataFactory.httpRequest('items?search='+$scope.searchText).then(function(data) { 
          $scope.itemAll = data.Alldata;
         });

      }else{
            dataFactory.httpRequest('itemsAll').then(function(data) {
            $scope.itemAll = data.Alldata;
            });

      }
    }
   
    $scope.array=[];
    $scope.addTocart = function (product) {
        if(localStorage.getItem('usersession')!=null)
        { 
          //alert(product.Item.itemId);
          $('#divid-'+product.Item.itemId).css('display','none')
          $scope.array.push(product);
           $scope.isDisabled = true;
           
        }else{

          alert('Login First to add itme into cart');

        }
        $('#cartvalue').html($scope.array.length);
        localStorage.setItem('cartata', JSON.stringify($scope.array));
        localStorage.setItem('cartlength', $scope.array.length);

  }




});

app.controller('CartController',function(dataFactory,$scope,$http){
         cartitme=localStorage.getItem('cartata');
         //console.log(cartitme);
         $scope.itemAll = JSON.parse(cartitme);
         $scope.removeItem=function($index)
         {
           //var index = $scope.itemAll.indexOf(product);
           $scope.itemAll.splice($index, 1);
            if($scope.itemAll.length>0)
            {
            $('.shopping-cart').css('display','block');
            //$scope.itemlengtharray=false;
            }else{
            $('.shopping-cart').css('display','none');
            $scope.message="Cart Empty Please Add Lab test";
            }

       
           localStorage.setItem('cartlength', $scope.itemAll.length);
           $('#cartvalue').html($scope.itemAll.length);
           localStorage.setItem('cartata', JSON.stringify($scope.itemAll));
         }
        $scope.getTotal = function() {
        var total = 0;
        for (var i = 0; i < $scope.itemAll.length; i++) {
          var product = $scope.itemAll[i];
           total +=product.Item.minPrice*1;
        }
        return total;
      }

      $scope.checkout=function(itemsarray,subtotal,grandtotal)
       {
         usesession=JSON.parse(localStorage.getItem('usersession'));
         userid=usesession[0].id;
          dataFactory.httpRequest('itemscheckout','POST',{},{'itemsarray':itemsarray,'subtotal':subtotal,'grandtotal':grandtotal,'userid':userid}).then(function(data) {
          if(data.insertitem==1)
             {
                $scope.message="Your Lab Test Order Sucessfully.";
                $('#checkout').css('display','none');
                $('.shopping-cart').css('display','none');
                localStorage.removeItem('cartata');
                 localStorage.removeItem('cartlength');
                 $('#cartvalue').html(0);
             }
          //console.log(data);

          });
       }
       $scope.itemlengtharray=$scope.itemAll.length;
       //alert($scope.itemlengtharray);
       if($scope.itemlengtharray>0)
       {
         $('.shopping-cart').css('display','block');
        //$scope.itemlengtharray=false;
       }else{
         $('.shopping-cart').css('display','none');
        $scope.message="Cart Empty Please Add Lab test";
       }

})




app.controller('ItemController', function(dataFactory,$scope,$http){
 
  $scope.data = [];
  $scope.pageNumber = 1;
  $scope.libraryTemp = {};
  $scope.totalItemsTemp = {};

  $scope.totalItems = 0;
  $scope.pageChanged = function(newPage) {
    getResultsPage(newPage);
  };

  getResultsPage(1);
  function getResultsPage(pageNumber) {
      if(! $.isEmptyObject($scope.libraryTemp)){
          dataFactory.httpRequest('/items?search='+$scope.searchText+'&page='+pageNumber).then(function(data) {
            $scope.data = data.data;
            $scope.totalItems = data.total;
            $scope.pageNumber = pageNumber;
          });
      }else{
        dataFactory.httpRequest('/items?page='+pageNumber).then(function(data) {
          $scope.data = data.data;
          $scope.totalItems = data.total;
          $scope.pageNumber = pageNumber;
        });
      }
  }

  $scope.searchDB = function(){
      if($scope.searchText.length >= 3){
          if($.isEmptyObject($scope.libraryTemp)){
              $scope.libraryTemp = $scope.data;
              $scope.totalItemsTemp = $scope.totalItems;
              $scope.data = {};
          }
          getResultsPage(1);
      }else{
          if(! $.isEmptyObject($scope.libraryTemp)){
              $scope.data = $scope.libraryTemp ;
              $scope.totalItems = $scope.totalItemsTemp;
              $scope.libraryTemp = {};
          }
      }
  }

  $scope.saveAdd = function(){
    dataFactory.httpRequest('itemsCreate','POST',{},$scope.form).then(function(data) {
      $scope.data.push(data);
      $(".modal").modal("hide");
    });
  }

  $scope.edit = function(id){
    dataFactory.httpRequest('itemsEdit/'+id).then(function(data) {
    	console.log(data);
      	$scope.form = data;
    });
  }

  $scope.saveEdit = function(){
    dataFactory.httpRequest('itemsUpdate/'+$scope.form.id,'PUT',{},$scope.form).then(function(data) {
      	$(".modal").modal("hide");
        $scope.data = apiModifyTable($scope.data,data.id,data);
    });
  }

  $scope.remove = function(item,index){
    var result = confirm("Are you sure delete this item?");
   	if (result) {
      dataFactory.httpRequest('itemsDelete/'+item.id,'DELETE').then(function(data) {
          $scope.data.splice(index,1);
      });
    }
  }
   
});