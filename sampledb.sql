-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `health_cart`;
CREATE TABLE `health_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_price` float NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `health_lab_order`;
CREATE TABLE `health_lab_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `labtest` varchar(200) NOT NULL,
  `subtotal` float NOT NULL,
  `grandtotal` float NOT NULL,
  `userid` int(11) NOT NULL,
  `order_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `health_lab_order` (`id`, `labtest`, `subtotal`, `grandtotal`, `userid`, `order_date`) VALUES
(4,	'[\"4\",\"5\"]',	17620,	17623.6,	1,	'2020-08-09'),
(5,	'[\"4\",\"5\"]',	17620,	17623.6,	1,	'2020-08-09');

DROP TABLE IF EXISTS `health_lab_test`;
CREATE TABLE `health_lab_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` text,
  `keywords` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `health_lab_test` (`id`, `package_name`, `keywords`) VALUES
(2,	'{\"S.no\":19956,\"itemId\":\"DIANM11\",\"itemName\":\"COVID-19 Test\",\"type\":\"Test\",\"Keyword\":\"covid-19-test\",\"Best-sellers\":\"\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"covid-19-test\",\"minPrice\":4500,\"labName\":\"Metropolis\",\"fasting\":0,\"availableAt\":1,\"popular\":\"TRUE\",\"category\":\"path\",\"objectID\":\"6045500\",\"_highlightResult\":{\"itemName\":{\"value\":\"COVID-19 Test\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"covid-19-test\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"path\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'covid-19-test,COVID-19 Test,covid'),
(3,	'{\"S.no\":1995,\"itemId\":\"DIA2044\",\"itemName\":\"Eye Test- Vision Express\",\"type\":\"Test\",\"Keyword\":\"eye,test\",\"Best-sellers\":\"\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"eye_test\",\"minPrice\":49,\"labName\":\"Vision Express\",\"fasting\":0,\"availableAt\":1,\"popular\":\"TRUE\",\"category\":\"path\",\"objectID\":\"4562\",\"_highlightResult\":{\"itemName\":{\"value\":\"Eye Test- Vision Express\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"eye,test\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"path\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'eye,test,eye'),
(4,	'{\"S.no\":1983,\"itemId\":\"DIAR894\",\"itemName\":\"Yttrium Therapy\",\"type\":\"Test\",\"Keyword\":\",Therapy\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"Yttrium-Therapy-test-cost\",\"minPrice\":17500,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461302\",\"_highlightResult\":{\"itemName\":{\"value\":\"Yttrium Therapy\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"Yttrium,Therapy\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Wrist,Lateral,Therapy'),
(5,	'{\"S.no\":1982,\"itemId\":\"DIAR893\",\"itemName\":\"X Ray Wrist Lateral View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Wrist,Lateral,View\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Wrist-Lateral-View-test-cost\",\"minPrice\":120,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461292\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Wrist Lateral View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Wrist,Lateral,View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Wrist,Lateral'),
(6,	'{\"S.no\":1981,\"itemId\":\"DIAR892\",\"itemName\":\"X Ray Wrist AP View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Wrist,AP,View\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Wrist-AP-View-test-cost\",\"minPrice\":120,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461282\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Wrist AP View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Wrist,AP,View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Wrist,AP,View'),
(7,	'{\"S.no\":1980,\"itemId\":\"DIAR891\",\"itemName\":\"X Ray Wrist AP and Lateral View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Wrist,AP,and\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Wrist-AP-and-Lateral-View-test-cost\",\"minPrice\":240,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461272\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Wrist AP and Lateral View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Wrist,AP,and\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Wrist,AP,and'),
(8,	'{\"S.no\":1979,\"itemId\":\"DIAR890\",\"itemName\":\"X Ray Whole Spine Lateral View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Whole,Spine,Lateral\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Whole-Spine-Lateral-View-test-cost\",\"minPrice\":320,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461262\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Whole Spine Lateral View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Whole,Spine,Lateral\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Whole,Spine,Lateral'),
(9,	'{\"S.no\":1978,\"itemId\":\"DIAR889\",\"itemName\":\"X Ray Whole Spine Lateral and AP View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Whole,Spine,Lateral\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Whole-Spine-Lateral-and-AP-View-test-cost\",\"minPrice\":560,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461252\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Whole Spine Lateral and AP View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Whole,Spine,Lateral\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Whole,Spine,Lateral'),
(10,	'{\"S.no\":1977,\"itemId\":\"DIAR888\",\"itemName\":\"X Ray Whole Spine AP View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Whole,Spine,AP\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Whole-Spine-AP-View-test-cost\",\"minPrice\":320,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461242\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Whole Spine AP View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Whole,Spine,AP\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Whole,Spine,AP'),
(11,	'{\"S.no\":1976,\"itemId\":\"DIAR887\",\"itemName\":\"X Ray Water View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Water,View,\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Water-View-test-cost\",\"minPrice\":145,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461232\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Water View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Water,View,\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Water,View'),
(12,	'{\"S.no\":1975,\"itemId\":\"DIAR886\",\"itemName\":\"X Ray Tm Joint Lateral View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Tm,Joint,Lateral\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Tm-Joint-Lateral-View-test-cost\",\"minPrice\":162,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461222\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Tm Joint Lateral View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Tm,Joint,Lateral\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Tm,Joint,Lateral'),
(13,	'{\"S.no\":1974,\"itemId\":\"DIAR885\",\"itemName\":\"X Ray Tm Joint AP View\",\"type\":\"Test\",\"Keyword\":\"X,Ray,Tm,Joint,AP\",\"testCount\":1,\"Included Tests\":\"\",\"url\":\"X-Ray-Tm-Joint-AP-View-test-cost\",\"minPrice\":162,\"labName\":\"\",\"fasting\":0,\"availableAt\":2,\"popular\":\"false\",\"category\":\"radio\",\"objectID\":\"4461212\",\"_highlightResult\":{\"itemName\":{\"value\":\"X Ray Tm Joint AP View\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Keyword\":{\"value\":\"X,Ray,Tm,Joint,AP\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"Included Tests\":{\"value\":\"\",\"matchLevel\":\"none\",\"matchedWords\":[]},\"category\":{\"value\":\"radio\",\"matchLevel\":\"none\",\"matchedWords\":[]}}}',	'X,Ray,Tm,Joint,AP,x,ray');

DROP TABLE IF EXISTS `health_user`;
CREATE TABLE `health_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `health_user` (`id`, `username`, `password`, `date`) VALUES
(1,	'singhtanwarajay@gmail.com',	'admin@123',	'2020-08-06 13:50:14');

-- 2020-08-09 12:02:47
