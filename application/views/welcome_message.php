<html lang="en">
<head>
	<title>Codeigniter 3</title>

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">


    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/pogo-slider.min.css">
	<!-- Site CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/custom.css">




	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	
	<!-- Angular JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>  
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route.min.js"></script>

	<!-- MY App -->
	<script src="app/packages/dirPagination.js"></script>
	<script src="app/routes.js"></script>
	<script src="app/services/myServices.js"></script>
	<script src="app/helper/myHelper.js"></script>

    <!-- ALL PLUGINS -->
	<script src="public/js/jquery.magnific-popup.min.js"></script>
    <script src="public/js/jquery.pogo-slider.min.js"></script> 
	<script src="public/js/slider-index.js"></script>
	<script src="public/js/smoothscroll.js"></script>
	<script src="public/js/TweenMax.min.js"></script>
	<script src="public/js/main.js"></script>
	<script src="public/js/owl.carousel.min.js"></script>
	<script src="public/js/form-validator.min.js"></script>
    <script src="public/js/contact-form-script.js"></script>
	<script src="public/js/isotope.min.js"></script>	
	<script src="public/js/images-loded.min.js"></script>	
    <script src="public/js/custom.js"></script>


	<!-- App Controller -->
	<script src="app/controllers/ItemController.js"></script>

</head>
<body ng-app="main-App" id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">
	<div class="main-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="left-top">
						<a class="new-btn-d br-2" href="#"><span>Book Appointment</span></a>
						<div class="mail-b"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> demo@gmail.com</a></div>
					</div>
				</div>
				<div class="col-lg-6">
					<!-- <div class="wel-nots">
						<p>Welcome to Our Health Lab!</p>
					</div> -->
					<div class="right-top">
						<ul style="margin-top: 10px;">
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End top bar -->
	
	<!-- Start header -->
	<header class="top-header">
		<nav class="navbar header-nav navbar-expand-lg">
            <div class="container">
				<a class="navbar-brand" href="index.html"><img src="public/images/logo.png" alt="image"></a>
				<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
					<span></span>
					<span></span>
					<span></span>
				</button> -->  
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link active" href="#/">Home</a></li>
                        <li><a class="nav-link" href="#alltest">All Lab Test Package</a></li>
                       <!--  <li><a class="nav-link" href="#services">Services</a></li>
						<li><a class="nav-link" href="#appointment">Appointment</a></li>
                        <li><a class="nav-link" href="#gallery">Gallery</a></li>-->
						<li><a class="nav-link" href="#team">Doctor</a></li>
                        <li><a class="nav-link" href="#blog">Blog</a></li>
						<li><a class="nav-link" href="#contact">Contact</a></li> 
						<li id="Login"><a class="nav-link" href="#login">Login</a></li> 
						<li style="display: none;" id="logout"><a class="nav-link" href="javascript:void(0)" onclick="logoutuser();" >Logout</a></li> 
						<li><a onclick="//go_tocart()" href="#/cart"  class="nav-link" ><img src="https://icons-for-free.com/iconfiles/png/512/cart-131964784999299812.png" style="width: 50px;"> <div id="cartvalue" style="background: red;height: 24px;width: 24px;border-radius: 12px;position: absolute;bottom: 24px;left: 52px;color: #FFF;font-size: 19px;padding: 8px;line-height: 14px;">0</div> </a></li>
                    </ul>
                </div>
            </div>
        </nav>
	</header>
		<ng-view></ng-view>

</body>
<script type="text/javascript">
	function logoutuser() {
		localStorage.removeItem("usersession");
		$('#logout').css('display','none');
         $('#Login').css('display','block');
		window.location.href="#/";
	}

   function go_tocart()
	 {
	 	 window.location.href="#/cart";
	 }

$( document ).ready(function() {
	 usersession=localStorage.getItem('usersession');
	 cartlength=localStorage.getItem('cartlength');
	 if(localStorage.getItem('usersession')!=null)
	 {
	 	 $('#logout').css('display','block');
         $('#Login').css('display','none');
	 }


	  if(localStorage.getItem('cartlength')!=null)
	 {
	 	 $('#cartvalue').html(cartlength);
	 }



	})
</script>
</html>
