<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
	
		parent::__construct();
		$this->load->database();
	}

	public function index()
	{
		$this->load->database();

		if(!empty($this->input->get("search"))){
			$this->db->like('keywords', $this->input->get("search"));
			//$this->db->or_like('description', $this->input->get("search")); 
		}

		//$this->db->limit(5, ($this->input->get("page",1) - 1) * 5);
		$query = $this->db->get("health_lab_test");

		$result = $query->result();

		$array=array();
		foreach ($result as $key => $value) {
			$array[$key]['id']=$value->id;
			$array[$key]['Item']=json_decode($value->package_name);

		}
	
		echo json_encode(['success'=>true,'Alldata'=>$array]);
	}



   public function itemscheckout()
    {

    	$this->load->database();
    	$_POST = json_decode(file_get_contents('php://input'), true);
    	$insert = $this->input->post();
    	$itemid=array();
    	$i=0;
    	foreach ($insert['itemsarray'] as $key => $value) {
    	$itemid[$i]=$value['id'];
    	$i++;
    	}
    	$today=date('Y-m-d');
    	$allorderitem = array('grandtotal'=>$insert['grandtotal'],'subtotal'=>$insert['subtotal'],'labtest'=>json_encode($itemid),'userid'=>$insert['userid'],'order_date'=>$today);
    	 $this->db->insert('health_lab_order', $allorderitem);
    	echo json_encode(['success'=>true,'insertitem'=>1]);

    }

	public function store()
    {
    	//error_reporting(0);
    	$this->load->database();
    	$_POST = json_decode(file_get_contents('php://input'), true);
    	$insert = $this->input->post();
    	$username=$insert['username'];
    	$password=$insert['password'];
    	$q = $this->db->get_where('health_user', array('username' => $username,'password'=>$password));
    	$result=$q->result();

    	if(!empty($result))
    	{
    		$uselogin=1;
    	}else{
    		$uselogin=0;
    	}
		echo json_encode(['success'=>true,'userid'=>$result,'userlogin'=>$uselogin]);
    }

    public function edit($id)
    {
    	$this->load->database();

		$q = $this->db->get_where('items', array('id' => $id));
		echo json_encode($q->row());
    }

    public function itemsAll()
     {

     	$query = $this->db->get("health_lab_test");
		$result=$query->result();

		$array=array();
		foreach ($result as $key => $value) {
			$array[$key]['id']=$value->id;
			$array[$key]['Item']=json_decode($value->package_name);

		}
	
		echo json_encode(['success'=>true,'Alldata'=>$array]);

     	//echo json_encode(['success'=>true]);
     }

     public  function insertintotable()
       {
       	$this->load->database();  
       	$myalltest = file_get_contents('https://5f1a8228610bde0016fd2a74.mockapi.io/getTestList');
       	$data = json_decode($myalltest, true);
       	foreach ($data as $key => $value) {
       		  $labtest=json_encode($value);
       		  $array= array('package_name' =>$labtest);
       		  $this->db->insert('health_lab_test', $array);
       	}

       	echo "done";

      
       }

    public function update($id)
    {
    	$this->load->database();
    	$_POST = json_decode(file_get_contents('php://input'), true);

    	$insert = $this->input->post();
    	$this->db->where('id', $id);
    	$this->db->update('items', $insert);

        $q = $this->db->get_where('items', array('id' => $id));
		echo json_encode($q->row());
    }

    public function delete($id)
    {
    	$this->load->database();
        $this->db->where('id', $id);
		$this->db->delete('items');
		echo json_encode(['success'=>true]);
    }
}
